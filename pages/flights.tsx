import React from 'react'
import { Row, Col, Spin, Button } from 'antd'
import { TravelCloudClient, formatCurrency, Cart } from 'travelcloud-antd'
import { FlightsParamsForm } from 'travelcloud-antd/components/flights-params-form'
import { FlightsResult } from 'travelcloud-antd/components/flights-result'
import { NoResult } from 'travelcloud-antd/components/no-result'
import config from '../customize/config'
import Head from 'next/head'
import Router from 'next/router'

export default class extends React.PureComponent<{cart: Cart, query}> {
  private client = new TravelCloudClient(config.tcUser);

  static async getInitialProps (context) {
    const query = context.query
    return { query }
  }

  state = {
    flights: {} as any,
    loading: false,
    ap1: this.props.query.ap1 || 'SIN',
    ap2: this.props.query.ap2,
    date1: this.props.query.date1,
    date2: this.props.query.date2,
    adult: this.props.query.adult || 1,
    child: this.props.query.child || 0,
    infant: this.props.query.infant || 0,
    cabin: this.props.query.cabin || 'Y'
  }

  async componentDidMount() {
    // update state with loading true
    this.setState({loading: true})

    // search flights with default state
    const flights = await this.client.flights(this.state)

    // update state with flights
    this.setState({loading: false, flights})
  }

  async paramsChange(value) {
    // update url
    Router.push({
      pathname: Router.pathname,
      query: value
    })

    // update state with filter values and loading true
    this.setState({loading: true, ...value})

    // search flights with filter values
    const flights = await this.client.flights(value)

    // update state with flights and loading false
    this.setState({loading: false, flights})
  }

  render() {
    return (
      <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '48px', padding: 32}}>
        <Head>
          <title>{config.defaultTitle} | Flights</title>
        </Head>
        <Row gutter={32}>
          <Col span={6}>
            <h1>Flights</h1>
            <FlightsParamsForm
              client={this.client}
              onChange={(value) => this.paramsChange(value)}
              value={this.state}
              defaultIataCodes={['SIN', 'BKK', 'KUL', 'HKG']}
              />
          </Col>
          <Col span={18}>
            {this.state.loading || this.state.flights.result == null || this.state.flights.result.length === 0
              ? <NoResult
                  style={{paddingTop: 128}}
                  response={this.state.flights}
                  loading={this.state.loading}
                  type="flights">
                  Please enter your destination and dates on the left
                </NoResult>
              : <FlightsResult flights={this.state.flights.result} onFlightClick={(flight) => this.props.cart.addFlight(flight, this.state, true)  && Router.push('/checkout')} />}
          </Col>
        </Row>
      </div>
    )
  }
}
