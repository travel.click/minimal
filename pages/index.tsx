import React from 'react'
import { Button } from 'antd'
import { TravelCloudClient, validateLegacyHotelsParams, validateFlightsParams } from 'travelcloud-antd'
import { LegacyHotelsParamsForm } from 'travelcloud-antd/components/legacy-hotels-params-form'
import { FlightsParamsForm } from 'travelcloud-antd/components/flights-params-form'
import config from '../customize/config'
import Router from 'next/router'

export default class extends React.PureComponent<any> {
  private client = new TravelCloudClient(config.tcUser);

  static async getInitialProps (context) {
    const query = context.query
    return { query }
  }

  state = {
    hotelsParams: {
      cityCode: null,
      checkInDate: null,
      checkOutDate: null,
      adults: 2,
      children: 0
    },
    hotelsParamsInvalid: true,

    flightsParams: {
      ap1: null,
      ap2: null,
      date1: null,
      date2: null,
      adult: 1,
      child: 0,
      infant: 0
    },
    flightsParamsInvalid: true,
  }

  render() {
    return (
      <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '48px', padding: 32}}>
        <h1>Welcome to Trip My Feet</h1>
        <h2>Hotel search</h2>
        <LegacyHotelsParamsForm
          client={this.client}
          onChange={(value) => this.setState({hotelsParams: value, hotelsParamsInvalid: validateLegacyHotelsParams(value) == null})}
          value={this.state.hotelsParams}
          defaultCityCodes={['SIN', 'BKK', 'KUL', 'HKG']}>
          <LegacyHotelsParamsForm.CityCodeSelect style={{width: 200}} placeholder="Destination" />
          <LegacyHotelsParamsForm.CheckInDatePicker style={{width: 200}} placeholder="Check-in" />
          <LegacyHotelsParamsForm.CheckOutDatePicker style={{width: 200}}  placeholder="Check-out" />
          <LegacyHotelsParamsForm.AdultOccupancySelect style={{width: 200}} />
        </LegacyHotelsParamsForm>
        <Button
          type="primary"
          disabled={this.state.hotelsParamsInvalid}
          onClick={() => Router.push({
            pathname: '/hotels',
            query: this.state.hotelsParams
          })}>
          Search
        </Button>

        <h2>Flight search</h2>
        <FlightsParamsForm
          client={this.client}
          onChange={(value) => this.setState({flightsParams: value, flightsParamsInvalid: validateFlightsParams(value) == null})}
          value={this.state.flightsParams}
          defaultIataCodes={['SIN', 'BKK', 'KUL', 'HKG']}>
          <FlightsParamsForm.OriginAirportSelect style={{width: 200}} placeholder="Origin" />
          <FlightsParamsForm.DestinationAirportSelect style={{width: 200}} placeholder="Destination" />
          <FlightsParamsForm.DepartureDatePicker style={{width: 200}} placeholder="Departure" />
          <FlightsParamsForm.ReturnDatePicker style={{width: 200}} placeholder="Return" />
          <FlightsParamsForm.AdultSelect style={{width: 200}} />
          <FlightsParamsForm.ChildSelect style={{width: 200}} />
          <FlightsParamsForm.InfantSelect style={{width: 200}} />
        </FlightsParamsForm>
        <Button
          type="primary"
          disabled={this.state.flightsParamsInvalid}
          onClick={() => Router.push({
            pathname: '/flights',
            query: this.state.flightsParams
          })}>
          Search
        </Button>
      </div>
    )
  }
}