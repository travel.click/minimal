import React from 'react'
import { Row, Col, Carousel } from 'antd'
import { TravelCloudClient, updateFormState } from 'travelcloud-antd'
import config from '../customize/config'
import {TourBookingForm, initialTourBookingForm} from 'travelcloud-antd/components/tour-booking'
import Router from 'next/router'

export default class extends React.PureComponent<any, any> {
  state: any = {
    tourBookingForm: initialTourBookingForm()
  }

  static async getInitialProps (context) {
    const client = new TravelCloudClient(config.tcUser)
    const tour = await client.tour(context.query)
    const query = context.query
    return { tour, query }
  }

  componentDidMount() {
    this.setState({
      tourBookingForm: initialTourBookingForm()
    })
  }

  render() {
    const tour = this.props.tour.result

    return (
      <div>
        <Row style={{margin: '32px 64px'}}>
          <h1 style={{fontSize: 40, fontWeight: 'normal', marginBottom: 0}}>{tour.name}</h1>
        </Row>
        <Row style={{backgroundColor: '#fff', margin: '32px 64px'}}>
          <Col span={12}>
            <Carousel autoplay>
              <div key='cover'><div style={{
                backgroundImage: `url(${tour.photo_url})`,
                height: '600px',
                width: '100%',
                backgroundSize: 'cover',
                backgroundPosition: 'center'}} /></div>
              {tour.photos.map((photo, index) =>
                <div key={index}><div style={{
                  backgroundImage: `url(${photo.url})`,
                  height: '600px',
                  width: '100%',
                  backgroundSize: 'cover',
                  backgroundPosition: 'center'}} /></div>)}
            </Carousel>
          </Col>
          <Col span={12} style={{height: '100%'}}>
            <TourBookingForm value={this.state.tourBookingForm} tour={tour} onChange={updateFormState(this, 'tourBookingForm')} onSubmit={(e) => this.props.cart.addTour(tour, this.state.tourBookingForm, true) && Router.push('/checkout') && e.preventDefault()} />
          </Col>
        </Row>
        <Row gutter={64} style={{backgroundColor: '#fff', margin: '32px 64px', padding: 32}}>
          <Col span={12}>
            <h1>Highlights</h1>
            <div dangerouslySetInnerHTML={{__html: tour.short_desc}} />
            <h1>Extras</h1>
            <div dangerouslySetInnerHTML={{__html: tour.extras}} />
            <h1>Remarks</h1>
            <div dangerouslySetInnerHTML={{__html: tour.remarks}} />
          </Col>
          <Col span={12}>
            <h1>Itinerary</h1>
            <div dangerouslySetInnerHTML={{__html: tour.itinerary}} />
          </Col>
        </Row>
      </div>
    )
  }
}
