import { CheckoutForm, initialCheckoutForm } from 'travelcloud-antd/components/checkout'
import { updateFormState } from 'travelcloud-antd'
import { Order } from 'travelcloud-antd/components/order'
import { Layout, Row, Col, Spin, Card } from 'antd'
import React from 'react'
import Router from 'next/router'

export default class extends React.PureComponent<any, any> {
  state: any = {
    loading: false
  }

  static getDerivedStateFromProps(props, currentState) {
    if (props.order != null && currentState.checkoutForm == null) {
      return {
        checkoutForm: initialCheckoutForm(props.order.result)
      }
    }
    return null
  }

  async onSubmit(e) {
    this.setState({
      loading: true
    })
    const result = await this.props.cart.addCheckout(this.state.checkoutForm, e)
    if (result.result == null) {
      this.setState({
        loading: false
      })
    } else {
      Router.push('/payment?ref=' + result.result.ref)
    }
  }

  render() {
    const cart = this.props.cart
    const order = this.props.order.result

    if (order == null) {
      return <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '64px auto', padding: 64, maxWidth: 1600}}>
        <Card loading={true} style={{border: 0}}></Card>
      </div>
    }

    if (order.products == null || order.products.length === 0) {
      return <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '64px auto', padding: 64, maxWidth: 1600}}>
        <h1>Your cart is empty</h1>
      </div>
    }

    return (
      <Layout style={{backgroundColor: '#fff', margin: '64px auto', padding: 64, maxWidth: 1600}}>
        <Row>
          <Col span={12}>
            <h1>Contact Details</h1>
            <CheckoutForm loading={this.state.loading} order={order} value={this.state.checkoutForm} onChange={updateFormState(this, 'checkoutForm')} onSubmit={(e) => this.onSubmit(e)} />
          </Col>
          <Col span={12}>
            <h1>Order Details</h1>
            <Order order={order} showSection={{products: true}} cart={cart} />
          </Col>
        </Row>
      </Layout>
  )}
}