import App, { Container } from 'next/app'
import React from 'react'
import Head from 'next/head'
import Link from 'next/link'
import * as NProgress from 'nprogress/nprogress'
import { Layout, Menu, Icon, Button } from 'antd'
import '../customize/styles.less'
import Router from 'next/router'
import config from '../customize/config'
import { Cart, getStateFromLocalStorage, TravelCloudClient } from 'travelcloud-antd'
import { Order } from 'travelcloud-antd/components/order'
import { Account } from 'travelcloud-antd/components/account'

const { Header, Content, Footer, Sider } = Layout;

Router.onRouteChangeStart = () => {NProgress.start();}
Router.onRouteChangeComplete = () => {NProgress.done();}
Router.onRouteChangeError = () => {NProgress.done();}

export default class MyApp extends App {
  state = {
    // for cart
    order: {loading: true} as any,
    customer: {loading: true},

    // for _app
    collapsed: true,
    siderAction: ''
  }
  client: TravelCloudClient = new TravelCloudClient(config.tcUser)
  cart: Cart = new Cart(this, this.client)

  componentDidMount() {
    this.setState(getStateFromLocalStorage())
  }

  openCart = async () => {
    this.setState({
      siderAction: 'cart',
      collapsed: false
    })
  }
  openAccount = async () => {
    this.setState({
      siderAction: 'account',
      collapsed: false
    })

    await this.cart.refreshCustomer()
  }
  closeCart = () => {
    if (this.state.collapsed === false) {
      this.setState({
        siderAction: {},
        collapsed: true,
      })}
  }

  render () {
    const { Component, pageProps } = this.props
    const cartIsEmpty = this.state.order.result == null || this.state.order.result.products == null || this.state.order.result.products.length === 0

    return (
      <Container>
        <Head>
          <meta name='viewport' content='width=device-width, initial-scale=1' />
          <meta charSet='utf-8' />
          <link rel='stylesheet' href='/_next/static/style.css' />
          <title>{config.defaultTitle}</title>
        </Head>
        <Layout hasSider={true}>
          <Layout className={this.state.collapsed === false && 'tc-dim-no-scroll-bars'} onClick={this.closeCart} style={{ overflow: 'hidden' }}>
            <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
              <div className="logo">
                Trip My Feet
              </div>
              <Menu
                theme="dark"
                mode="horizontal"
                style={{ lineHeight: '64px', borderBottom: 0 }}>
                <Menu.Item key="0">
                  <Link href="/">
                    <a>Home</a>
                  </Link>
                </Menu.Item>
                <Menu.Item key="1">
                  <Link href="/tours">
                    <a>Tours</a>
                  </Link>
                </Menu.Item>
                <Menu.Item key="2">
                  <Link href="/generics">
                    <a>Tickets</a>
                  </Link>
                </Menu.Item>
                <Menu.Item key="3">
                  <Link href="/hotels">
                    <a>Hotels</a>
                  </Link>
                </Menu.Item>
                <Menu.Item key="4">
                  <Link href="/flights">
                    <a>Flights</a>
                  </Link>
                </Menu.Item>
                <Menu.Item key="cart" style={{float: 'right'}} onClick={() => this.openCart()}>
                  <Icon type="shopping-cart"  style={{fontSize: 16}} /> Cart
                </Menu.Item>
                <Menu.Item key="customer" style={{float: 'right'}} onClick={() => this.openAccount()}>
                  <Icon type="user"  style={{fontSize: 16}} /> My Account
                </Menu.Item>
              </Menu>
            </Header>
            <Content style={{ padding: '64px 0px', minHeight: 1000 }}>
              <Component client={this.client} customer={this.state.customer} order={this.state.order} cart={this.cart} openCart={() => this.openCart()} {...pageProps} />
            </Content>
            <Footer style={{ textAlign: 'center' }}>
              {config.companyName} ©2018
            </Footer>
          </Layout>
          <Sider
            className="cart-sider"
            collapsedWidth={0}
            collapsible
            collapsed={this.state.collapsed}
            theme="light"
            width={700}
            trigger={null}
            style={{ width: 700, overflow: 'auto', height: '100vh', position: 'fixed', right: 0, zIndex: 2, boxShadow: '-1px 0 5px 0px rgba(0, 0, 0, 0.2), -4px 0 10px 0px rgba(0, 0, 0, 0.1)' }}>
            <div style={{padding: "32px", position: "relative", width: 700}}>
              <Icon type="close"  style={{fontSize: 24, position: 'absolute', top: 32, right: 32, cursor: 'pointer'}} onClick={this.closeCart} />
              {this.state.siderAction === 'cart'
              &&  <div>
                    <div style={{paddingBottom: 16, fontSize: 24, paddingRight: 16, lineHeight: "32px"}}>Shopping Cart</div>
                    {cartIsEmpty
                      ? <div>Your cart is empty</div>
                      : [
                        <Order order={this.state.order.result} showSection={{products: true, remove: true}} cart={this.cart} key={1} />,
                        <Button
                          key={2}
                          onClick={() => {
                            this.setState({collapsed: true})
                            Router.push('/checkout')
                          }}
                          style={{marginTop: 16, float: 'right'}}
                          type="primary">Proceed to checkout <Icon type="right" /></Button>]
                      }</div>}
              {this.state.siderAction === 'account'
              &&  <Account customer={this.state.customer} cart={this.cart} />}
            </div>
          </Sider>
        </Layout>
      </Container>
    )
  }
}
