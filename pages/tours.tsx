import React from 'react'
import { Row, Col, Menu } from 'antd'
import { TravelCloudClient } from 'travelcloud-antd'
import { ToursResult } from 'travelcloud-antd/components/tours-result'
import { NoResult } from 'travelcloud-antd/components/no-result'
import config from '../customize/config'
import Head from 'next/head'
import Router from 'next/router'

export default class extends React.Component<any, any> {
  static async getInitialProps (context) {
    const client = new TravelCloudClient(config.tcUser)
    const tours = await client.tours(Object.assign({'categories.name': '11D'}, context.query))
    const query = context.query
    const categories = await client.categories({category_type: 'tour_package'})
    return { tours, query, categories }
  }

  state = {
    loading: false,
    'categories.name': this.props.query['categories.name'] || '11D'
  }

  componentDidUpdate(prevProps) {
    if (prevProps.tours !== this.props.tours) {
      this.setState({
        loading: false
      })
    }
  }

  menuClick({item, key}) {
    // update url
    Router.push({
      pathname: Router.pathname,
      query: {'categories.name': key}
    })

    // update state with filter values and loading true
    this.setState({loading: true, "categories.name": key})
  }

  onTourClick(tour) {
    Router.push({
      pathname: '/tour',
      query: {id: tour.id}
    })
    this.setState({
      loading: true
    })
  }

  render() {
    return (
      <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '48px', padding: 32}}>
        <Head>
          <title>{config.defaultTitle} | Tours</title>
        </Head>
        <Row gutter={32}>
          <Col span={6}>
            <h1>Tours</h1>
            <Menu
              selectedKeys={[this.state['categories.name']]}
              onClick={(x) => this.menuClick(x)}
              mode="vertical">
              {this.props.categories.result != null
                &&this.props.categories.result.map(category =>
                  <Menu.Item key={category.name}>
                    <span>{category.name}</span>
                  </Menu.Item>)}
            </Menu>
          </Col>
          <Col span={18}>
            {this.state.loading || this.props.tours.result == null || this.props.tours.result.length === 0
              ? <NoResult
                  style={{paddingTop: 128}}
                  response={this.props.tours}
                  loading={this.state.loading}
                  type="tours" />
              : <ToursResult
                  toursResult={this.props.tours.result}
                  onTourClick={(tour) => this.onTourClick(tour)} />}
          </Col>
        </Row>
      </div>
    )
  }
}