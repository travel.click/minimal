import React from 'react'
import { Row, Col, Spin, Carousel } from 'antd'
import { TravelCloudClient, formatCurrency } from 'travelcloud-antd'
import { NoResult } from 'travelcloud-antd/components/no-result'
import config from '../customize/config'
import Head from 'next/head'
import Router from 'next/router'
import Link from 'next/link'

export default class extends React.PureComponent<any> {
  private client = new TravelCloudClient(config.tcUser);

  static async getInitialProps (context) {
    const query = context.query
    return { query }
  }

  state = {
    hotel: {} as any,
    loading: true,
    hotelId: this.props.query.hotelId,
    checkInDate: this.props.query.checkInDate,
    checkOutDate: this.props.query.checkOutDate,
    adults: this.props.query.adults || 2,
    children: this.props.query.children || 0
  }

  async componentDidMount() {
    // update state with loading true
    this.setState({loading: true})

    // search hotel with default state
    //const hotel = await this.client.legacyHotel(this.state)

    // update state with hotel
    this.setState({loading: false, hotel: true})
  }

  async paramsChange(value) {
    // update url
    Router.push({
      pathname: Router.pathname,
      query: value
    })

    // update state with filter values and loading true
    this.setState({loading: true, ...value})

    // search hotel with filter values
    //const hotel = await this.client.legacyHotel(value)

    // update state with hotel and loading false
    this.setState({loading: false, hotel: true})
  }

  render() {
    const hotel = this.state.hotel.result
    return (
      <div>
        <Head>
          <title>{config.defaultTitle} | Hotel</title>
        </Head>
        {this.state.loading && false
          ? <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '48px', padding: 32}}>
              <NoResult
                style={{paddingTop: 128}}
                response={this.state.hotel}
                loading={this.state.loading}
                type="hotel">
                Cannot load selected hotel. Please&nbsp;
                <Link href={{ pathname: '/hotel', query: {
                  cityCode: this.props.query.cityCode,
                  checkInDate: this.props.query.checkInDate,
                  checkOutDate: this.props.query.checkOutDate,
                  adults: this.props.query.adults || 2,
                  children: this.props.query.children || 0
                    }}}>
                  <a>select a different hotel</a>
                </Link>
                &nbsp;or different dates.
              </NoResult>
            </div>
          : <div style={{maxWidth: 1400, margin: '32px auto'}}>
              <Row>
                <h1 style={{fontSize: 40, fontWeight: 'normal', marginBottom: 0}}>Hotel name</h1>
              </Row>
              <Row style={{backgroundColor: '#fff', margin: '32px 0'}}>
                <Col span={8}>
                  {/*
                  <Carousel autoplay>
                    {hotel.images.map((photo, index) =>
                      <div key={index}><div style={{
                        backgroundImage: `url(${photo.image})`,
                        height: '400px',
                        width: '100%',
                        backgroundSize: 'cover',
                        backgroundPosition: 'center'}} /></div>)}
                  </Carousel>
                      */}
                  <div style={{
                        backgroundImage: `url(http://www.hotelbeds.com/giata/06/069607/069607a_hb_l_019.jpg)`,
                        height: '400px',
                        width: '100%',
                        backgroundSize: 'cover',
                        backgroundPosition: 'center'}} />
                </Col>
                <Col span={16}>
                  <div style={{overflow: 'hidden', height: '400px', width: '100%', position: 'relative'}}>
                    <div style={{
                      backgroundImage: `url(https://cdn.net.in/static/staticmap.png)`,
                      position: 'absolute',
                      top: '50%',
                      left: '50%',
                      margin: '-500px',
                      height: '1000px',
                      width: '1000px',
                      backgroundSize: 'cover',
                      backgroundPosition: 'center'}} />
                  </div>
                </Col>
              </Row>
              <Row gutter={64} style={{backgroundColor: '#fff', margin: '32px 0', padding: 32}}>
                <Col span={12}>
                  <h1>Highlights</h1>
                </Col>
                <Col span={12}>
                  <h1>Itinerary</h1>
                </Col>
              </Row>
            </div>}
      </div>
    )
  }
}